#include <string>
#include <iostream>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

extern unsigned char spi_mode;
extern unsigned char bits_per_word;
extern unsigned int speed;
extern const char *device;
extern int spi_fd;

int SpiOpenPort();
int SpiClosePort();

