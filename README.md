# **Lepton-camera** #

A git repo for code related to flir lepton , a LWIR camera with a resolution of 80*60 px , whihc uses VoSPI ( Video over SPI ) , so easily interfaced with arm based MCUs and other development board like Raspberry pi.

leptonSDKEmb32PUB -->  Folder containing flir's sdk and i2c code for raspberry-pi. Compile with : make

lepton_gst.cpp ---> Example code using appsrc to push frames in a gstreamer pipeline
		|
		----------> Compile with : g++ lepton_gst.cpp spi.cpp gpio.cpp lepton_i2c.cpp -o lepton_gst `pkg-config --cflags --libs gstreamer-1.0 gstreamer-app-1.0 gstreamer-video-1.0` -LleptonSDKEmb32PUB/Debug -lLEPTON_SDK
		|
		----------> Run : ./lepton_gst  ( as root )

