#include <gst/gst.h>
#include <gst/app/gstappsink.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <pthread.h>
#include <unistd.h>

#include "spi.h"
#include "gpio.h"
#include "lepton_i2c.h"

#define PACKET_SIZE 164 // 4 bytes header , 160 bytes payload
#define PACKETS_PER_FRAME 60 // Raw 14bit mode , Telemetry disabled ( as footer or header declared later ) , AGC enabled
#define PACKET_SIZE_UINT16 (PACKET_SIZE/2)
#define FRAME_SIZE_UINT16 (PACKET_SIZE_UINT16*PACKETS_PER_FRAME)

static int width = 80;
static int height = 60;
int packet_number;

// Buffer for incoming data

static uint8_t tmp_buf[PACKET_SIZE*PACKETS_PER_FRAME]; 
uint8_t rawdata[60*80];

// Flags to indicate to parent thread that GstreamerThread started and finished
static volatile bool GstreamerThreadStarted = false;

static GstElement *appsrc;
static GstElement *sink;

gboolean bus_call(GstBus *bus, GstMessage *msg, gpointer data)
{
	GMainLoop *loop = (GMainLoop *) data;

	switch (GST_MESSAGE_TYPE (msg))
	{

	case GST_MESSAGE_EOS:
		fprintf(stderr, "End of stream\n");
		g_main_loop_quit(loop);
		break;

	case GST_MESSAGE_ERROR:
	{
		gchar *debug;
		GError *error;

		gst_message_parse_error(msg, &error, &debug);
		g_free(debug);

		g_printerr("Error: %s\n", error->message);
		g_error_free(error);

		g_main_loop_quit(loop);

		break;
	}
	default:
		break;
	}

	return TRUE;
}


void* GstreamerThread(void* pThreadParam)
{
	GMainLoop *loop;
	GstElement *pipeline, *videoconvert;
	GstBus *bus;
	guint bus_watch_id;

	pipeline = gst_pipeline_new("mypipeline");
	appsrc = gst_element_factory_make("appsrc", "mysource");
	videoconvert = gst_element_factory_make("videoconvert", "videoconvert");
	sink = gst_element_factory_make("ximagesink", "mysink");

	// Check if all elements were created
	if (!pipeline || !appsrc || !videoconvert || !sink )
	{
		fprintf(stderr, "Could not gst_element_factory_make, terminating\n");
		return (void*)0xDEAD;
	}

	GstCaps *caps; 
	caps = gst_caps_new_simple("video/x-raw", "format",
		G_TYPE_STRING, "GRAY8", "width", G_TYPE_INT, width, "height",
		G_TYPE_INT, height, "framerate", GST_TYPE_FRACTION, 0, 1, NULL);


	g_object_set(G_OBJECT (appsrc), "blocksize", 4800, NULL);
	g_object_set(G_OBJECT (appsrc), "do-timestamp", TRUE, NULL);

	loop = g_main_loop_new(NULL, FALSE);

	bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
	bus_watch_id = gst_bus_add_watch(bus, bus_call, loop);
	gst_object_unref(bus);

	gst_bin_add_many(GST_BIN(pipeline), appsrc, videoconvert, sink, NULL);

	gst_element_link_filtered(appsrc, videoconvert, caps);
	gst_element_link_many(videoconvert,sink,NULL);

	fprintf(stderr, "Setting g_main_loop_run to GST_STATE_PLAYING\n");

	gst_element_set_state(pipeline, GST_STATE_PLAYING);

	// Indicate that thread was started
	GstreamerThreadStarted = true;


	g_main_loop_run(loop);

	fprintf(stderr, "g_main_loop_run returned, stopping playback\n");


	gst_element_set_state(pipeline, GST_STATE_NULL);

	fprintf(stderr, "Deleting pipeline\n");

	gst_object_unref(GST_OBJECT(pipeline));

	g_source_remove (bus_watch_id);
	g_main_loop_unref (loop);

	return NULL;
}

// Starts GstreamerThread
bool StartGstreamer()
{
	unsigned long GtkThreadId;
	pthread_attr_t GtkAttr;

	// Start thread
	int result = pthread_attr_init(&GtkAttr);
	if (result != 0)
	{
		fprintf(stderr, "pthread_attr_init returned error %d\n", result);
		return false;
	}

	void* pParam = NULL;
	result = pthread_create(&GtkThreadId, &GtkAttr,
			GstreamerThread, pParam);
	if (result != 0)
	{
		fprintf(stderr, "pthread_create returned error %d\n", result);
		return false;
	}

	return true;
}

// Puts raw data in appsrc for encoding into gstreamer. Size must put exactly width*height bytes.
void PushBuffer()
{

	for(int i=0 ; i<PACKETS_PER_FRAME ; i++ ){

		read(spi_fd, tmp_buf+(sizeof(uint8_t)*PACKET_SIZE*i), sizeof(uint8_t)*PACKET_SIZE );

		packet_number = tmp_buf[(i*PACKET_SIZE)+1];

		if(packet_number != i){
			i=-1;
		}

	}

	int p=0;

	for(int i=1 ; i<(PACKET_SIZE*PACKETS_PER_FRAME) ; i += 2){

		if( i % PACKET_SIZE < 4){
			continue;
		}
		rawdata[p] = tmp_buf[i];
		p++;

	}

	GstFlowReturn ret;
	GstBuffer *buffer;

	int size = 80*60;
	buffer = gst_buffer_new_allocate(NULL, size, NULL);

	GstMapInfo info;
	gst_buffer_map(buffer, &info, GST_MAP_WRITE);
	uint8_t* buf = info.data;
	memcpy(buf, rawdata, size);
	gst_buffer_unmap(buffer, &info);

	ret = gst_app_src_push_buffer(GST_APP_SRC(appsrc), buffer);

	if( ret != GST_FLOW_OK ){
		fprintf(stderr,"Error pushing buffer \n");
	}

}

int main(int argc, char *argv[])
{

	printf("Testing Gstreamer-1.0 Jpeg encoder.\n");


	gpio_state(8,1);
	usleep(200); // wait > 185ms to resync
	gpio_state(8,0);

	SpiOpenPort();

	gst_init(NULL, NULL); 

	width = 80;
	height = 60;

	StartGstreamer();

	usleep(2000);

	while(!GstreamerThreadStarted){
		usleep(0);
	}

	while(1)
	{

		// Push raw buffer into gstreamer
		PushBuffer();

		//fprintf(stderr, "Could not read compressed data from gstreamer\n");
		usleep(50000);
	}

	//Tell Gstreamer thread to stop, pushing EOS into gstreamer
	gst_app_src_end_of_stream(GST_APP_SRC(appsrc));

	printf("Exiting \n");

	SpiClosePort();

	return 0;

}
