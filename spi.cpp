#include "spi.h"


unsigned char spi_mode = SPI_MODE_3 ;	//SPI MODE : CPOL=1 CPHA=1
unsigned char bits_per_word = 8 ;	//Bits per word
unsigned int speed = 10000000 ;		//SPI speed : 10MHz
const char *device = "/dev/spidev0.0" ;	//SPI device

int spi_fd ;

int SpiOpenPort(){

	int status = -1;

	std::cout<<"Opening file descriptor"<<std::endl;

	spi_fd = open( device , O_RDWR);

	if(spi_fd < 0)
	{
		perror(std::string("Couldn't open SPI port " ).c_str() );
		exit(1);
	}

	status = ioctl(spi_fd, SPI_IOC_RD_MODE , &spi_mode );
	if(status <0)
	{
		perror(std::string("Couldn't set SPI read MODE " ).c_str() );
		exit(1);
	}
	

	status = ioctl(spi_fd, SPI_IOC_RD_BITS_PER_WORD , &bits_per_word );
	if(status<0)
	{
		perror(std::string("Couldn't set SPI bits per word (read)").c_str() );
		exit(1);
	}

	status = ioctl(spi_fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed );
	if(status<0)
	{
		perror(std::string("Couldn't set SPI clock " ).c_str() );
	}
	
	std::cout<<"All done"<<std::endl;

	return 0;

}

int SpiClosePort(){

	int status = -1;

	std::cout<<"Closing file descriptor"<<std::endl;

	status = close(spi_fd);
	if(status<0){
		perror(std::string("Couldn't close SPI port on : ").c_str() );
		exit(1);
	}

	std::cout<<"Fd closed"<<std::endl;

	return 0;

}
	

