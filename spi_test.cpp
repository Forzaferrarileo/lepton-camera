#include <iostream>
#include "spi.h"
#include "unistd.h"

using namespace std;

int main(){

	cout<<"Starting...."<<endl;

	if( SpiOpenPort() == 0 ){
		cout<<"Spi port opened"<<endl;
	}

	sleep(3);
	
	if( SpiClosePort() == 0){
		cout<<"Spi port closed"<<endl;
	}

	return 0;

}
